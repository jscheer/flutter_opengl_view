// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_opengl_view/flutter_opengl_view.dart';

void main() => runApp(const MyApp());

/// This example app shows, how to integrate a FlutterOpenglView plugin into the
/// widget tree.
///
/// In addition, an Overlay shows that other widgets can be rendered on top of
/// the FlutterOpenglView.
///
/// Note: Overlaying other widgets only works on Android, if the GLTextureView
/// is used.

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FlutterOpenglView openGlView =
        const FlutterOpenglView(useTextureView: true);

    return MaterialApp(
      home: Scaffold(
        body: SizedBox(
          width: 300.0,
          height: 330.0,
          child: Overlay(initialEntries: [
            OverlayEntry(builder: (context) {
              return openGlView;
            }),
            OverlayEntry(builder: (context) {
              return Column(children: const [
                Text(
                  'Hello Flutter World',
                )
              ]);
            })
          ]),
        ),
      ),
    );
  }
}
