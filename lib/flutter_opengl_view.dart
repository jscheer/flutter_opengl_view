// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/rendering.dart';

/// The FlutterOpenglView initializes an openGL context for rendering.
/// The widget acts as a canvas for all openGL calls.
///
/// Besides native IOS and Android openGL calls, a C++ API is also provided.
///
/// Android Notes:
/// Setting useTextureView=true, creates a TextureView, otherwise a
/// GLSurfaceView will be created. The GLSurfaceView does not allow to draw
/// other widgets on top.
class FlutterOpenglView extends StatefulWidget {
  /// The c++ pointer of a FlutterOpenGL (FlutterOpenGL.h) class.
  /// Similar to: flutter_opengl_view/ios/Classes/cpp/include/FlutterOpenGL.h
  final int cPlusPlusPointer;

  /// If true, a TextureView is created. If false, a GLSurfaceView is created.
  /// Note: only has an effect on Android.
  final bool useTextureView;

  /// Creates the openGL view.
  const FlutterOpenglView(
      {Key? key,

      /// The c++ pointer of a FlutterOpenGL (FlutterOpenGL.h) class
      this.cPlusPlusPointer = 0,

      /// If true, a TextureView is created. If false, a GLSurfaceView is created.
      this.useTextureView = true})
      : super(key: key);

  @override
  State<FlutterOpenglView> createState() => _FlutterOpenglViewState();
}

class _FlutterOpenglViewState extends State<FlutterOpenglView> {
  final String _viewType = 'flutter_opengl_view';

  // Pass parameters to the platform side.
  final Map<String, dynamic> _creationParams = <String, dynamic>{};

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _creationParams["cpp"] = widget.cPlusPlusPointer;
    _creationParams["useTextureView"] = widget.useTextureView;

    if (Theme.of(context).platform == TargetPlatform.android) {
      // Return AndroidViewSurface/PlatformViewLink for Android
      return PlatformViewLink(
          viewType: _viewType,
          surfaceFactory:
              (BuildContext context, PlatformViewController controller) {
            return AndroidViewSurface(
              controller: controller as AndroidViewController,
              gestureRecognizers: const <
                  Factory<OneSequenceGestureRecognizer>>{},
              hitTestBehavior: PlatformViewHitTestBehavior.opaque,
            );
          },
          onCreatePlatformView: (PlatformViewCreationParams params) {
            return PlatformViewsService.initSurfaceAndroidView(
              id: params.id,
              viewType: _viewType,
              layoutDirection: TextDirection.ltr,
              creationParams: _creationParams,
              creationParamsCodec: StandardMessageCodec(),
            )
              ..addOnPlatformViewCreatedListener(params.onPlatformViewCreated)
              ..create();
          });
    } else if (Theme.of(context).platform == TargetPlatform.iOS) {
      // Return UiKitView for iOS
      return UiKitView(
          viewType: _viewType,
          layoutDirection: TextDirection.ltr,
          creationParams: _creationParams,
          creationParamsCodec: const StandardMessageCodec());
    }

    return new Text('Platform not yet supported by this plugin');
  }
}
