// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'flutter_opengl_view_platform_interface.dart';

/// An implementation of [FlutterOpenglViewPlatform] that uses method channels.
class MethodChannelFlutterOpenglView extends FlutterOpenglViewPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('flutter_opengl_view_channel');
}
