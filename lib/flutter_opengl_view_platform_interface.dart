// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'flutter_opengl_view_method_channel.dart';

abstract class FlutterOpenglViewPlatform extends PlatformInterface {
  /// Constructs a FlutterOpenglViewPlatform.
  FlutterOpenglViewPlatform() : super(token: _token);

  static final Object _token = Object();

  static FlutterOpenglViewPlatform _instance = MethodChannelFlutterOpenglView();

  /// The default instance of [FlutterOpenglViewPlatform] to use.
  ///
  /// Defaults to [MethodChannelFlutterOpenglView].
  static FlutterOpenglViewPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FlutterOpenglViewPlatform] when
  /// they register themselves.
  static set instance(FlutterOpenglViewPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }
}
