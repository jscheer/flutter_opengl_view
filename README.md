# flutter_opengl_view

The flutter openGL plugin enables openGL rendering on Android and iOS. <br>
<br>
With this plugin, you can use native (Android, iOS) openGL calls as well as <br>
C++ openGL code.<br>

# Usage
## Add plugin in pubspec.yaml:
``` yaml
# pubspec.yaml

dependencies:
  flutter:
    sdk: flutter

  flutter_opengl_view: ^0.0.1
 
```

## Add plugin in Flutter code:
```dart
import 'package:flutter_opengl_view/flutter_opengl_view.dart';

Widget build(BuildContext context) {
  
  /* use a ffi call to your c++ code, to get a proper pointer.
   * myCppPointer = 0 will use ios/android openGL rendering, but not c++. 
   */
  int myCppPointer = 0; 
  
  FlutterOpenglView openGlView = const FlutterOpenglView(
      cPlusPlusPointer: myCppPointer,
      useTextureView: true
  );

  return MaterialApp(
    home: Scaffold(
      body: SizedBox(
        width: 300.0,
        height: 300.0,
        child: openGlView // Add the openGL plugin in arbitrary parts of the widget tree.
      ),
    ),
  );
}
```

- If your render with native iOS/Android code (objc/Java), `cPlusPlusPointer` can be set to zero.
- `useTextureView = true` (default) uses a `TextureView` on Android, while `useTextureView = false` uses an `GLSurfaceView`.
  - `GLSurfaceView`s can not be rotated, for example but lead to higher performance. In addition, they do not allow to properly overlap with other views or widgets.

# Own C++ class
To use your own c++ opengl code, you must extend the class [`FlutterOpenGL`](https://gitlab.com/jscheer/flutter_opengl_view/-/blob/master/ios/Classes/cpp/include/FlutterOpenGL.h):
``` c++ 
# header for MyFlutterOpenGL class:

class MyFlutterOpenGL : public FlutterOpenGL {

public:

    MyFlutterOpenGL();

    virtual ~FlutterGL();

    void init(int width, int height);

    void stopDrawFrame();

    void onDrawFrame();

    bool isReady();
};
```

`onDrawFrame()` is continually called by the openGL thread. <br>
`init()` is called by the openGL thread as well and can be used to initialize resources and configure openGL.

## C++ / Dart Example Interface
Interfacing c++ from dart can look as follows:
### C++ Part:
On the c++ side, you have to obtain a pointer to an `FlutterOpenGL` instance:
``` c++
#include <cstdint>

#if __cplusplus

#if defined(_WIN32)
#define DART_EXPORT extern "C" __declspec(dllexport)
#else
#define DART_EXPORT                                                            \
  extern "C" __attribute__((visibility("default"))) __attribute((used))
#endif

// Returns a pointer of an FlutterOpenGL object, casted to int64_t.
DART_EXPORT int64_t getFlutterOpenGL() {
  MyFlutterOpenGL* flutterOpenGl = new MyFlutterOpenGL();
  
  int64_t flutterOpenGlReference = reinterpret_cast<int64_t>(flutterOpenGl);
  return flutterOpenGlReference;
}

#endif  // __cplusplus

```

### Dart Part:
Dart retrieves the c++ pointer (previously casted to Int64) and hands it over to the constructor.
``` dart
final DynamicLibrary myFfiLibrary = Platform.isAndroid
    ? DynamicLibrary.open("myFfiLibrary.so")    // same name as in CMake file
    : DynamicLibrary.process();

/// Returns a pointer to an FlutterOpenGL object and can be handed over to the dart constructor of
/// FlutterOpenglView().
final int Function() getFlutterOpenGL = myFfiLibrary
    .lookup<NativeFunction<Int64 Function()>>("getFlutterOpenGL")
    .asFunction();
```

# More Information:
## Use C++ code in Android
In your own plugin or application, add the c++ source code destination to your `build.gradle`. <br>
Check [Flutter c-interop page](https://docs.flutter.dev/development/platform-integration/android/c-interop#step-2-add-cc-sources) for more details. <br>

## use C++ for IOS in XCode
- XCode: Select "Compile Source As" variable in compiler settings and set its value to "Objective-C++"
- change Obj-C file to .mm  
- add c++ sources within XCode: Runner -> Target -> Build Phases

Check[Flutter c-interop page](https://docs.flutter.dev/development/platform-integration/ios/c-interop#step-2-add-cc-sources) for more details. <br>

## Usage from another plugin:
- create flutter plugin that will contain your C++ openGL code
  flutter create -a java -i objc --template=plugin <myNewPlugin>

- add flutter_opengl_view to your created plugin
  (in pubspec.yaml, dependencies section)

- add Key to <myPlugin>/example/ios/Runner/Info.plist
  <key>io.flutter.embedded_views_preview</key>
  <true/>

# Contact
for more information, feel free to contact me: dev @ jscheer.de
