// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import "FlutterOpenglViewFactory.h"
#import "FlutterOpenglView.h"

@implementation FlutterOpenglViewFactory {
  NSObject<FlutterBinaryMessenger>* _messenger;
}

- (instancetype)initWithMessenger:(NSObject<FlutterBinaryMessenger>*)messenger {
  self = [super init];
  if (self) {
    _messenger = messenger;
  }
  return self;
}

- (NSObject<FlutterMessageCodec>*)createArgsCodec {
  return [FlutterStandardMessageCodec sharedInstance];
}

- (NSObject<FlutterPlatformView>*)createWithFrame:(CGRect)frame
                                   viewIdentifier:(int64_t)viewId
                                        arguments:(id _Nullable)args {

  FlutterOpenGlViewController* flutterOpenGlViewController
                  = [[FlutterOpenGlViewController alloc] initWithFrame:frame
                            viewIdentifier:viewId
                            arguments:args
                            binaryMessenger:_messenger];

  return flutterOpenGlViewController;
}

@end
