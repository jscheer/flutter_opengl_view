// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import "FlutterOpenglViewPlugin.h"
#import "FlutterOpenglViewFactory.h"

@implementation FlutterOpenglViewPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {

  FlutterOpenglViewFactory* openGlviewFactory =
      [[FlutterOpenglViewFactory alloc] initWithMessenger:registrar.messenger];

  [registrar registerViewFactory:openGlviewFactory withId:@"flutter_opengl_view"];
}

@end
