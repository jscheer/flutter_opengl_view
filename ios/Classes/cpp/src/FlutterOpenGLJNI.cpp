// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifdef __ANDROID__

#include <FlutterOpenGLJNI.h>
#include <FlutterOpenGL.h>


JNIEXPORT void JNICALL Java_com_example_flutter_1opengl_1view_FlutterRendererJNI_init(JNIEnv * env, jobject obj, jlong flutterOpenGL, jint width, jint height)
{
  FlutterOpenGL* openGL = reinterpret_cast<FlutterOpenGL*>(flutterOpenGL);
  openGL->init(width, height);
}

JNIEXPORT void JNICALL Java_com_example_flutter_1opengl_1view_FlutterRendererJNI_onDrawFrame(JNIEnv * env, jobject obj, jlong flutterOpenGL)
{
  FlutterOpenGL* openGL = reinterpret_cast<FlutterOpenGL*>(flutterOpenGL);
  openGL->onDrawFrame();
}

JNIEXPORT void JNICALL Java_com_example_flutter_1opengl_1view_FlutterRendererJNI_stopDrawFrame(JNIEnv * env, jobject obj, jlong flutterOpenGL)
{
  FlutterOpenGL* openGL = reinterpret_cast<FlutterOpenGL*>(flutterOpenGL);
  openGL->stopDrawFrame();
}

JNIEXPORT jboolean JNICALL Java_com_example_flutter_1opengl_1view_FlutterRendererJNI_isReady(JNIEnv * env, jobject obj, jlong flutterOpenGL)
{
  FlutterOpenGL* openGL = reinterpret_cast<FlutterOpenGL*>(flutterOpenGL);
  jboolean ready = openGL->isReady();
  return ready;
}

#endif
