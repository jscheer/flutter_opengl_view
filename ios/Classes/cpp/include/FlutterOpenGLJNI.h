// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifdef __ANDROID__

#include <jni.h>

#ifndef FLUTTER_PLUGIN_FLUTTEROPENGL_JNI_H
#define FLUTTER_PLUGIN_FLUTTEROPENGL_JNI_H

extern "C" {

JNIEXPORT void JNICALL Java_com_example_flutter_1opengl_1view_FlutterRendererJNI_init(JNIEnv * env, jobject obj, jlong flutterOpenGL, jint width, jint height);
JNIEXPORT void JNICALL Java_com_example_flutter_1opengl_1view_FlutterRendererJNI_onDrawFrame(JNIEnv * env, jobject obj, jlong flutterOpenGL);
JNIEXPORT void JNICALL Java_com_example_flutter_1opengl_1view_FlutterRendererJNI_stopDrawFrame(JNIEnv * env, jobject obj, jlong flutterOpenGL);
JNIEXPORT jboolean JNICALL Java_com_example_flutter_1opengl_1view_FlutterRendererJNI_isReady(JNIEnv * env, jobject obj, jlong flutterOpenGL);

};

#endif // FLUTTER_PLUGIN_FLUTTEROPENGL_JNI_H
#endif  // Android
