// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import "FlutterOpenglView.h"
#import <GLKit/GLKit.h>
#import "GlViewController.h"

@implementation FlutterOpenGlViewController {

  GlViewController* _glViewController;
  GLKView* _openGlView;
  UITextField* _textField;
  int64_t _viewId;
  FlutterMethodChannel* _channel;
}

- (instancetype)initWithFrame:(CGRect)frame
               viewIdentifier:(int64_t)viewId
                    arguments:(id _Nullable)args
              binaryMessenger:(NSObject<FlutterBinaryMessenger>*)messenger {
  if (self = [super init]) {

    _viewId = viewId;
    NSString* channelName = [NSString stringWithFormat:@"flutter_opengl_view_%lld", viewId];
    _channel = [FlutterMethodChannel methodChannelWithName:channelName binaryMessenger:messenger];

    _glViewController = [[GlViewController alloc] initWithNibName:nil bundle:nil];
    _openGlView = (GLKView*)[_glViewController getView];

    __weak __typeof__(self) weakSelf = self;
    [_channel setMethodCallHandler:^(FlutterMethodCall* call, FlutterResult result) {
      [weakSelf onMethodCall:call result:result];
    }];

  }

  return self;
}

- (UIView*)view {

  return _openGlView;
}

- (void)onMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {

//  int dummyWidth = 100;
//  int dummyHeight = 100;

  if ([[call method] isEqualToString:@"initGL"]) {

//      (void)[_glViewController init:dummyWidth viewPortHeight:dummyHeight];

      result(nil);
    } else if ([[call method] isEqualToString:@"initGLNative"]) {


        NSNumber* flutterOpenGLCppRef = call.arguments[@"cppRef"];
        [_glViewController setCppRef:flutterOpenGLCppRef.longValue];
//        (void)[_glViewController init:dummyWidth viewPortHeight:dummyHeight];

        result(nil);
    } else if ([[call method] isEqualToString:@"ready"]) {
        // TODO:
        result(nil);
    } else if ([[call method] isEqualToString:@"stop"]) {
        
        [_glViewController stop]; 
        result(nil);
  } else {
    result(FlutterMethodNotImplemented);
  }
}

@end
