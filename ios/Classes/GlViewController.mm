// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import "GlViewController.h"
// #include "nativeOpenGL.h"
#include "cpp/include/FlutterOpenGL.h"

@interface GlViewController () {

    GLKView* _view;
    FlutterOpenGL* _flutterOpenGlCppRef;
    bool _hasCppRef;

    bool _initialized;
}

@property (strong, nonatomic) EAGLContext *context;

- (void)setupGL;

@end

@implementation GlViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil
                         bundle:(NSBundle *)nibBundleOrNil
{

  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {

    _hasCppRef = false;
    _initialized = false;
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
      if (!self.context) {
          NSLog(@"Failed to create ES context");
      }

    _view = (GLKView *)self.view;
    _view.context = self.context;
    _view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
  }

  return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupGL];
}

- (void)dealloc
{
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

- (void)init:(int)width viewPortHeight:(int)height {

    if(_hasCppRef)
    {
        if(_flutterOpenGlCppRef != nullptr)
          _flutterOpenGlCppRef->init(width, height);
    }
}

- (void)setCppRef:(long)flutterOpenGLCppRef {

    if(flutterOpenGLCppRef == 0)
    {
        _flutterOpenGlCppRef = nullptr;
        _hasCppRef = false;
        return;
    }

    _flutterOpenGlCppRef = (FlutterOpenGL*)flutterOpenGLCppRef;
    _hasCppRef = true;
}

- (void)stop {

    if(_hasCppRef)
    {
        if(_flutterOpenGlCppRef != nullptr)
          _flutterOpenGlCppRef->stopDrawFrame();

        _flutterOpenGlCppRef = nullptr;
        _hasCppRef = false;    }
}

- (GLKView*)getView {

  return _view;
}


- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    if(_hasCppRef)
    {
        // TODO:
    } else {

        // my native ios setup code
    }
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    if(_hasCppRef)
    {
        if(!_initialized && _flutterOpenGlCppRef != nullptr) {
            _flutterOpenGlCppRef->init(0, 0);
            _initialized = true;
        }
        if(_flutterOpenGlCppRef != nullptr)
          _flutterOpenGlCppRef->onDrawFrame();
    } else {

        // my native ios drawing code
        glClearColor(1.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);
    }
}

@end
