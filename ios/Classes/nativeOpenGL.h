// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

void on_surface_created();

void on_surface_changed();

void on_draw_frame();
