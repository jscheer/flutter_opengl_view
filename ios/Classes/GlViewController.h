// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface GlViewController : GLKViewController

- (UIView*)getView;

- (void)setCppRef:(long)flutterOpenGLCppRef;

- (void)stop;

- (void)init:(int)width viewPortHeight:(int)height;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil
                         bundle:(NSBundle *)nibBundleOrNil;

@end
