// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.example.flutter_opengl_view;

import android.opengl.GLES10;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class FlutterRenderer implements GLSurfaceView.Renderer {

    private boolean mReady = false;

    public FlutterRenderer()
    { }

    protected void setReady()
    {
        mReady = true;
    }

    protected void setNotReady()
    {
        mReady = false;
    }

    public boolean isReady()
    {
        return mReady;
    }

    public void stop() {
        // TODO:
        // add implementation
    }

    public void onDrawFrame(GL10 gl) {

        // TODO: add openGL calls
        // e.g.: GLES10.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES10.glClear(GLES10.GL_COLOR_BUFFER_BIT);
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {

        mReady = true;
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        // Do nothing.
    }
}
