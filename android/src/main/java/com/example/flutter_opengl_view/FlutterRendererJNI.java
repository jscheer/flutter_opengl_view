// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.example.flutter_opengl_view;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class FlutterRendererJNI extends FlutterRenderer {

    // C++ reference, returned by native create method
    private long flutterOpenGLCppRef = 0;
    private boolean hasCppReference = false;
    private boolean initialized = false;


    static {
        System.loadLibrary("FlutterOpenGLJNI");
    }

    public FlutterRendererJNI()
    {

    }

    public void setCppRef(long openGLCppRef)
    {
        if(openGLCppRef == 0) {
            flutterOpenGLCppRef = 0;
            hasCppReference = false;
            return;
        }
        flutterOpenGLCppRef = openGLCppRef;
        hasCppReference = true;
    }

    public boolean isReady()
    {
        if(hasCppReference)
            return isReady(flutterOpenGLCppRef);

        return false;
    }

    @Override
    public void stop() {

        initialized = false;
        stopDrawFrame(flutterOpenGLCppRef);
        setCppRef(0);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        if(hasCppReference && initialized)
            onDrawFrame(flutterOpenGLCppRef);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {

        if(hasCppReference) {
            //setNotReady();
            init(flutterOpenGLCppRef, width, height);
            //setReady();

            initialized = true;
        }
    }

    /*
     * JNI methods
     *
     */
    private static native void init(long flutterOpenGLCppRef, int width, int height);

    private static native void onDrawFrame(long flutterRendererCPP);

    private static native void stopDrawFrame(long flutterRendererCPP);

    private static native boolean isReady(long flutterOpenGLCppRef);
}
