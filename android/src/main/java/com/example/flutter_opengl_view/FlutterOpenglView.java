// Copyright 2022 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.example.flutter_opengl_view;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.flutter.plugin.platform.PlatformView;
import java.util.Map;
import android.view.ViewGroup;

public class FlutterOpenglView implements PlatformView {

    private RelativeLayout relativeLayout;

    private boolean useTextureView;

    private GLTextureView glTextureView;

    private GLView glView;

    public FlutterOpenglView(@NonNull Context context, int id, @Nullable Map<String, Object> creationParams)
    {
        // get input parameters
        Object cppPointerObj = creationParams.get("cpp");
        useTextureView = (boolean) creationParams.get("useTextureView");

        // cast cppPointerObj to int or long, depending on the input type
        long cppPointer = 0L;
        if(cppPointerObj instanceof Integer) {

            int cppPointerInt = (int) cppPointerObj;
            cppPointer = Integer.valueOf(cppPointerInt).longValue();
        } else if(cppPointerObj instanceof Long) {

            cppPointer = ((Long) cppPointerObj).longValue();
        }

        // create surrounding layout
        relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        // Create FlutterRenderer or FlutterRendererJNI, if cppPointer is present.
        FlutterRenderer flutterRenderer;

        if(cppPointer == 0) {
            flutterRenderer = new FlutterRenderer();
        }
        else {
            FlutterRendererJNI renderer = new FlutterRendererJNI();
            renderer.setCppRef(cppPointer);
            flutterRenderer = renderer;
        }

        // Create GLView (GLSurfaceView) or GLTextureView (TextureView), depending on
        // textureView parameter.
        if(useTextureView) {
            glTextureView = new GLTextureView(context);
            glTextureView.setRenderer(flutterRenderer);
            relativeLayout.addView(glTextureView);
        }
        else {
            glView = new GLView(context, flutterRenderer);
            glView.setZOrderMediaOverlay(true);
            glView.setZOrderOnTop(false);
            glView.setPreserveEGLContextOnPause(false);
            relativeLayout.addView(glView);
        }
    }

    @Override
    public View getView() {

        return relativeLayout;
    }

    @Override
    public void dispose() {
    }
}
