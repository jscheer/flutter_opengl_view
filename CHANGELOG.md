## 0.0.3

* run formatter
* adjust example test

## 0.0.2

* style guide improvements to tackle publishing points on flutter pusblishing site 
  * change block comments to `///`
  * Simplify License file
  * add `lints: ^2.0.0` to `pubspec.yaml`
* ignore `pubspec.lock`

## 0.0.1

* Initial release